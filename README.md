# WDD Collector Library

Android Library that collect device information to perform quality diagnostics.


**How to use inside your Android Studio project:**


**1) Go to File -> New -> New Module -> Import .JAR/.AAR package**


**2) Add those 2 lines to your build.gradle dependencies:**

    implementation project(':Collector-release')
    implementation 'com.android.volley:volley:1.1.0'


**3) Add this method call to your onClick method button or anywhere you want to call**

    CollectData collectData = new CollectData("user", "password");
    collectData.collect(getActivity());

* Request your user and password to the technical team.
